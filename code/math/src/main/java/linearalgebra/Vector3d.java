/**
 * This class is for vectors.
 * @author David Hebert
 * @version 02/10/2023
 */
package linearalgebra;
public class Vector3d {
    private double x;
    private double y;
    private double z;
    public  Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
/**
 * method to get x
 * @return the value of x
 */
    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        double xSqrd = Math.pow(this.x, 2);
        double ySqrd = Math.pow(this.y, 2);
        double zSqrd = Math.pow(this.z, 2);
        return Math.sqrt(xSqrd +  ySqrd + zSqrd);
    }
/**
 * @param vector object of type vector 
 * @return the dot product of the values within the vector object
 */
    public double dotProduct(Vector3d vector){
        double x = vector.getX();
        double y = vector.getY();
        double z = vector.getZ();

        return (this.x * x) + (this.y * y) + (this.z * z);
    }
    
    public Vector3d add(Vector3d vector){
        double x = vector.getX();
        double y = vector.getY();
        double z = vector.getZ();

        Vector3d temp = new Vector3d(this.x + x,this.y + y,this.z + z);

        return temp;
    }
}
