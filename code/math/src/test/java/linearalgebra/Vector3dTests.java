package linearalgebra;
import linearalgebra.Vector3d;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class Vector3dTests {
    @Test
    public void answerShouldBe5(){
        Vector3d vector = new Vector3d(5, 3, 1);
        assertEquals(5.0, vector.getX(), 0);

    }

    @Test
     public void answerShouldBe3(){
        Vector3d vector = new Vector3d(5, 3, 1);
        assertEquals(3.0, vector.getY(), 0);
     }

     @Test
      public void answerShouldBe1(){
        Vector3d vector = new Vector3d(5, 3, 1);
        assertEquals(1.0, vector.getZ(), 0);
     }

     @Test
    public void answerShouldBe5p19(){
        Vector3d vector = new Vector3d(3, 3, 3);
        assertEquals(5.19, vector.magnitude(), 0.009);
    }

    @Test
    public void answerShouldBe26(){
        Vector3d vector1 = new Vector3d(1, 4, 3);
        Vector3d vector2 = new Vector3d(3, 2, 5);
        assertEquals(26.0, vector1.dotProduct(vector2), 0);
    }

     @Test
    public void answerShouldBe(){
        boolean worked = true;
        Vector3d vector1 = new Vector3d(1, 4, 3);
        Vector3d vector2 = new Vector3d(3, 2, 6);
        vector1 = vector1.add(vector2);
        if(vector1.getX() == 4.0 && vector1.getY() == 8.0 && vector1.getZ() == 9.0){
            worked = true;
        }
        else{
            worked = false;
        }
        assertEquals(true, worked);
    }
    
}
